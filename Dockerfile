# install php dependencies in intermediate container
FROM composer:latest AS composer

WORKDIR /var/www/html

COPY composer.* /var/www/html/

RUN composer global require hirak/prestissimo --no-plugins --no-scripts
RUN composer install --apcu-autoloader -o --no-dev --no-scripts --ignore-platform-reqs

# install admin javascript dependencies and build assets in intermediate container
FROM node:latest AS node-admin

COPY composer.json /var/www/html/
COPY assets/admin /var/www/html/assets/admin
COPY --from=composer /var/www/html/vendor/sulu/sulu /var/www/html/vendor/sulu/sulu
COPY --from=composer /var/www/html/vendor/friendsofsymfony/jsrouting-bundle /var/www/html/vendor/friendsofsymfony/jsrouting-bundle

RUN cd /var/www/html/assets/admin && npm install && NODE_OPTIONS="--max_old_space_size=4096" npm run build

# install website javascript dependencies and build assets in intermediate container
FROM node:latest AS node-website

COPY package* /var/www/html/
COPY webpack.config.js /var/www/html
COPY assets/website /var/www/html/assets/website

RUN cd /var/www/html && npm install && npm run build

# build actual application image
FROM php:7.3-apache AS apache

WORKDIR /var/www/html

# install packages
# inkscape is recommended for handling svg files with imagemagick
RUN apt-get update && apt-get install -y \
        openssl \
        git \
        unzip \
        cron \
        libzip-dev \
        libicu-dev \
        libmagickwand-dev \
        inkscape

# install PHP extensions
RUN docker-php-ext-configure intl && docker-php-ext-install -j$(nproc) \
        intl \
        pdo \
        pdo_mysql \
        opcache \
        zip

RUN pecl install imagick redis apcu && docker-php-ext-enable imagick redis apcu

# apache config
RUN /usr/sbin/a2enmod rewrite && /usr/sbin/a2enmod headers && /usr/sbin/a2enmod expires

# configure crontab
COPY ./deploy/config/cron.sh /cron.sh
RUN chmod +x /cron.sh
COPY ./deploy/config/crontab /etc/crontab
RUN crontab /etc/crontab

# copy needed files from build containers
COPY --from=node-admin /var/www/html/public/build/admin /var/www/html/public/build/admin
COPY --from=node-website /var/www/html/public/build/website /var/www/html/public/build/website
COPY --from=composer /var/www/html/vendor/ /var/www/html/vendor/

COPY . /var/www/html/

FROM apache AS production

# php config
ADD ./deploy/config/php.production.ini /usr/local/etc/php/conf.d/custom.ini

# apache config
COPY deploy/config/apache.production.conf /etc/apache2/sites-available/000-default.conf

FROM apache AS staging

# php config
ADD ./deploy/config/php.staging.ini /usr/local/etc/php/conf.d/custom.ini

# apache config
COPY ./deploy/config/.htpasswd /etc/apache2/.htpasswd
COPY deploy/config/apache.staging.conf /etc/apache2/sites-available/000-default.conf


FROM apache AS development

# php config
ADD ./deploy/config/php.development.ini /usr/local/etc/php/conf.d/custom.ini

# apache config
COPY ./deploy/config/.htpasswd /etc/apache2/.htpasswd
COPY deploy/config/apache.development.conf /etc/apache2/sites-available/000-default.conf
