<?php

declare(strict_types=1);

namespace App\Tests\Functional\Templates\Pages;

use App\Tests\Functional\BaseTestCase;
use App\Tests\Functional\Traits\PageTrait;

/**
 * Test default template.
 */
class DefaultTest extends BaseTestCase
{
    use PageTrait;

    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->initPhpcr();
    }

    /**
     * {@inheritdoc}
     */
    public function testDefault(): void
    {
        $locale = 'en';

        $this->createPage(
            'default',
            'example',
            [
                'title' => 'Neue Defaultseite',
                'url' => '/default',
                'published' => true,
            ],
            $locale
        );

        $client = $this->createWebsiteClient();
        $client->setServerParameter('HTTP_HOST', 'example.lo');
        $crawler = $client->request('GET', '/default');

        $this->assertHttpStatusCode(200, $client->getResponse());
        $this->assertCrawlerProperty($crawler, 'h1', 'Neue Defaultseite');
    }
}
