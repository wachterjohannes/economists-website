<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 * @ORM\Entity()
 */
class Subscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * First Name (required, published)
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $firstName;
    /**
     * Last Name (required, published)
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $lastName;
    /**
     * Email Address (required, NOT published)
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $emailAddress;
    /**
     * Academic background in economics or adjacent discipline (required, NOT published)
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $academicBackground;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $verificationToken;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isDataPolicyAccepted = false;
    /**
     * @var \DateTime $created
     *
     * @ORM\Column(type="datetime")
     */
    private $createdDate;
    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $verificationDate;

    public function __construct()
    {
        $this->firstName = '';
        $this->lastName = '';
        $this->emailAddress = '';
        $this->academicBackground = '';;
        $this->isVerified = 0;
        $this->verificationToken = '';
        $this->isDataPolicyAccepted = 0;
        $this->createdDate = new \DateTime();
        $this->verificationDate = new \DateTime();
    }

    public function toArray(): array
    {
        return [
            $this->id,
            $this->firstName,
            $this->lastName,
            $this->emailAddress,
            $this->academicBackground,
    
            $this->isVerified === false ? 0 : 1,
            $this->verificationToken,
            $this->isDataPolicyAccepted === true ? 1 : 0,
            $this->createdDate instanceof \DateTime ? $this->createdDate->format('Y-m-d H:i:s') : '',
            $this->verificationDate instanceof \DateTime ? $this->verificationDate->format('Y-m-d H:i:s') : '',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress(string $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * Get academic background in economics or adjacent discipline (required, NOT published)
     *
     * @return  string
     */ 
    public function getAcademicBackground()
    {
        return $this->academicBackground;
    }

    /**
     * Set academic background in economics or adjacent discipline (required, NOT published)
     *
     * @param  string  $academicBackground  Academic background in economics or adjacent discipline (required, NOT published)
     *
     * @return  self
     */ 
    public function setAcademicBackground(string $academicBackground)
    {
        $this->academicBackground = $academicBackground;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    /**
     * @param bool $isVerified
     */
    public function setIsVerified(bool $isVerified): void
    {
        $this->isVerified = $isVerified;
    }

    /**
     * @return string
     */
    public function getVerificationToken(): string
    {
        return $this->verificationToken;
    }

    /**
     * @param string $verificationToken
     */
    public function setVerificationToken(string $verificationToken): void
    {
        $this->verificationToken = $verificationToken;
    }

    /**
     * @return bool
     */
    public function isDataPolicyAccepted(): bool
    {
        return $this->isDataPolicyAccepted;
    }

    /**
     * @param bool $isDataPolicyAccepted
     */
    public function setIsDataPolicyAccepted(bool $isDataPolicyAccepted): void
    {
        $this->isDataPolicyAccepted = $isDataPolicyAccepted;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getVerificationDate(): \DateTime
    {
        return $this->verificationDate;
    }

    /**
     * @param \DateTime $verificationDate
     */
    public function setVerificationDate(\DateTime $verificationDate): void
    {
        $this->verificationDate = $verificationDate;
    }
}
