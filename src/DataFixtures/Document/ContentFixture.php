<?php

namespace App\DataFixtures\Document;

use Sulu\Component\DocumentManager\DocumentManager;
use Sulu\Bundle\DocumentManagerBundle\DataFixtures\DocumentFixtureInterface;
use Sulu\Component\DocumentManager\Exception\MetadataNotFoundException;
use Symfony\Component\Yaml\Parser;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class ContentFixture implements DocumentFixtureInterface
{
    /**
     * Simple local string with two chars.
     */
    const LOCALE = 'en';

    /**
     * All fixtures will be sorted in regards of the returned integer. This
     * "weight" will let a fixture run later if the integer is higher.
     *
     * @return int
     */
    public function getOrder()
    {
        return 10;
    }

    /**
     * Load fixtures.
     *
     * Use the document manager to create and save fixtures.
     * Be sure to call DocumentManager#flush() when you are done.
     *
     * @param DocumentManager $documentManager
     *
     * @throws MetadataNotFoundException
     */
    public function load(DocumentManager $documentManager)
    {
        $yaml = new Parser();
        $configFilePath = __DIR__ . '/../../Resources/data/static_pages.yml';
        $content = file_get_contents($configFilePath);
        if ($content === false) {
            throw new \Exception("'$configFilePath' could not be read.");
        }
        $data = $yaml->parse($content);
        foreach ($data['static'] as $overview) {
            /**
             * "page" is the base content of sulu. "article" for example would be used be the Article bundle.
             *
             * @var \Sulu\Bundle\PageBundle\Document\PageDocument $document
             */
            $document = $documentManager->create('page');

            // Set the local. Keep in mind that you have to save every local version extra.
            $document->setLocale(static::LOCALE);

            // The title of the page set in the template XML. Can not be set by getStructure()->bind();
            $document->setTitle($overview['title']);

            // Use setStructureType to set the name of the page template.
            if (isset($overview['template'])) {
                $document->setStructureType($overview['template']);
            } else {
                $document->setStructureType('default');
            }
            

            // URL of the content with out any language prefix.
            $document->setResourceSegment('/'.$overview['name']);

            // Data for all content types that this template uses.
            $document->getStructure()->bind(
                [
                    'article' => $overview['body'],
                ]
            );

            // Data for the "Excerpt & Taxonomies" tab when editing content.
            $document->setExtension(
                'excerpt',
                [
                    'title' => $overview['title'],
                    'description' => $overview['description'],
                    'categories' => [],
                    'tags' => [],
                ]
            );

            // Data for the "SEO" tab when editing content.
            $document->setExtension(
                'seo',
                [
                    'title' => $overview['title'],
                ]
            );

            // parent_path uses your webspace name. In this case "sulu_io"
            $documentManager->persist(
                $document,
                static::LOCALE,
                [
                    'parent_path' => '/cmf/ecologistsforfuture/contents',
                ]
            );

            // Optional: If you don't want your document to be published, remove this line
            $documentManager->publish($document, static::LOCALE);
        }
        // Persist immediately to database.
        $documentManager->flush();
    }
}
