<?php

namespace App\Controller\Campaign;

use App\Entity\Subscription;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class SubscriptionController extends AbstractController
{
    public function index(
        Request $request,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer,
        LoggerInterface $logger
    ): Response {
        $form = $this->createSubscriptionForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $token = $this->generateUuid();

            $logger->info('Verification token created: ' . $token);
            /** @var Subscription $subscription */
            $subscription = $form->getData();
            $existingSubscription = $entityManager->getRepository(Subscription::class)
                ->findOneBy(['emailAddress' => $subscription->getEmailAddress()]);
            if ($existingSubscription instanceof Subscription) {
                $logger->warning('Second try with same email address');

                return $this->render(
                    'pages/campaign/subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'error' => 'Mail-Address still exists: ' . $subscription->getEmailAddress(),
                    ]
                );
            }
            $subscription->setIsVerified(false);
            $subscription->setVerificationToken($token);
            $subscription->setCreatedDate(new \DateTime('now'));
            $entityManager->persist($subscription);
            $entityManager->flush();

            try {
                $result = $this->sendVerificationMail($mailer, $subscription, $token);
            } catch (\Swift_TransportException $e) {
                $logger->error(
                    'Problems to send mail',
                    [
                        'message' => $e->getMessage(),
                        'subscription_email' => $subscription->getEmailAddress(),
                        'subscription' => $subscription->toArray(),
                    ]
                );

                return $this->render(
                    'pages/campaign/subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'error' => 'Problems to send Mail. Please try again.',
                    ]
                );
            }
            if ($result) {
                $logger->info('Successful subscription - Mail send');
            } else {
                $logger->error('Problems to send mail');

                return $this->render(
                    'pages/campaign/subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'error' => 'Problems to send Mail. Please try again.',
                    ]
                );
            }

            return $this->redirectToRoute('successful_campaign_subscription');
        }

        return $this->render('pages/campaign/subscription.html.twig', ['form' => $form->createView()]);
    }

    public function successfulSubscription(): Response
    {
        return $this->render('pages/campaign/successful_subscription.html.twig', []);
    }

    private function createSubscriptionForm()
    {
        $subscription = new Subscription();
        $form = $this->createFormBuilder($subscription)
            ->add('firstName', TextType::class, ['label' => 'form.label.firstName', 'required' => true])
            ->add('lastName', TextType::class, ['label' => 'form.label.lastName', 'required' => true])
            ->add('emailAddress', EmailType::class, ['label' => 'form.label.emailAddress', 'required' => true])
            ->add(
                'academicBackground',
                ChoiceType::class,
                [
                    'label' => 'form.label.academicBackground',
                    'required' => true,
                    'choices' => [
                        'form.choice.label.no_academic_background' => 'no_academic_background',
                        'form.choice.label.student' => 'student',
                        'form.choice.label.researcher' => 'researcher',
                        'form.choice.label.working_professionell' => 'working_professionell',
                    ],
                    'expanded' => true,
                    'multiple' => false,
                ]
                )

            ->add(
                'isDataPolicyAccepted',
                CheckboxType::class,
                [
                    'required' => true,
                    'constraints' => [
                        new IsTrue(
                            [
                                'message' => 'I know, it\'s hard, but you must agree to our data policy.',
                            ]
                        ),
                    ],
                ]
            )
            ->add('submit', SubmitType::class, ['label' => 'form.label.submit'])
            ->getForm();

        return $form;
    }

    private function generateUuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0C2f) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0x2Aff),
            mt_rand(0, 0xffD3),
            mt_rand(0, 0xff4B)
        );
    }

    private function sendVerificationMail(\Swift_Mailer $mailer, Subscription $subscription, string $token): int
    {
        $message = (new \Swift_Message('Verification for Economists for Future Signature'))
            ->setFrom('economists.campaign@developersforfuture.org')
            ->setTo($subscription->getEmailAddress())
            ->setBody(
                $this->renderView(
                    'pages/campaign/email_verification.html.twig',
                    [
                        'subscription' => $subscription,
                        'token' => $token,
                    ]
                ),
                'text/html'
            );

        return $mailer->send($message);
    }
}
