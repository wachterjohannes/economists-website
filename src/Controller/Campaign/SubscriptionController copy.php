                                                                                                                            <?php

namespace App\Controller\Campaign;

use App\Entity\Subscription;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class SubscriptionController extends AbstractController
{
    public function index(
        Request $request,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer,
        LoggerInterface $logger
    ): Response {
        $form = $this->createSubscriptionForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $token = $this->generateUuid();

            $logger->info('Verification token created: ' . $token);
            /** @var Subscription $subscription */
            $subscription = $form->getData();
            $existingSubscription = $entityManager->getRepository(Subscription::class)
                ->findOneBy(['emailAddress' => $subscription->getEmailAddress()]);
            if ($existingSubscription instanceof Subscription) {
                $logger->warning('Second try with same email address');

                return $this->render(
                    'pages/campaign/subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'error' => 'Mail-Address still exists: ' . $subscription->getEmailAddress(),
                    ]
                );
            }
            $subscription->setIsVerified(false);
            $subscription->setVerificationToken($token);
            $subscription->setCreatedDate(new \DateTime('now'));
            $entityManager->persist($subscription);
            $entityManager->flush();

            try {
                $result = $this->sendVerificationMail($mailer, $subscription, $token);
            } catch (\Swift_TransportException $e) {
                $logger->error(
                    'Problems to send mail',
                    [
                        'message' => $e->getMessage(),
                        'subscription_email' => $subscription->getEmailAddress(),
                        'subscription' => $subscription->toArray(),
                    ]
                );

                return $this->render(
                    'pages/campaign/subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'error' => 'Problems to send Mail. Please try again.',
                    ]
                );
            }
            if ($result) {
                $logger->info('Successful subscription - Mail send');
            } else {
                $logger->error('Problems to send mail');

                return $this->render(
                    'pages/campaign/subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'error' => 'Problems to send Mail. Please try again.',
                    ]
                );
            }

            return $this->redirectToRoute('successful_campaign_subscription');
        }

        return $this->render('pages/campaign/subscription.html.twig', ['form' => $form->createView()]);
    }

    public function successfulSubscription(): Response
    {
        return $this->render('pages/campaign/successful_subscription.html.twig', []);
    }

    private function createSubscriptionForm()
    {
        $subscription = new Subscription();
        $form = $this->createFormBuilder($subscription)
            ->add('firstName', TextType::class, ['label' => 'form.label.firstName', 'required' => true])
            ->add('lastName', TextType::class, ['label' => 'form.label.lastName', 'required' => true])
            ->add('emailAddress', EmailType::class, ['label' => 'form.label.emailAddress', 'required' => true])
            ->add(
                'academicBackground',
                ChoiceType::class,
                [
                    'label' => 'form.label.academicBackground',
                    'required' => true,
                    'choices' => [
                        'form.choice.label.no_academic_background' => 'no_academic_background',
                        'form.choice.label.student' => 'student',
                        'form.choice.label.researcher' => 'researcher',
                        'form.choice.label.working_professionell' => 'working_professionell',
                    ],
                    'expanded' => false,
                    'multiple' => false,
                ]
                )



            ->add('titleBeforeName', TextType::class, ['label' => 'Title(s) before your name (optional, published)   	 e.g. Prof. / Dr.', 'required' => false])
            ->add('titleAfterName', TextType::class, ['label' => 'Title(s) after your name (optional, published)      	 e.g. PhD', 'required' => false])

            ->add(
                'highestAcademicDegree',
                ChoiceType::class,
                [
                    'label' => 'Highest academic degree in economists or adjacent discipline (required, NOT published)',
                    'required' => true,
                    'choices' => [
                        'Bachelor' => 'Bachelor',
                        'Master' => 'Master',
                        'PhD' => 'PhD',
                        'Dr' => 'Dr',

                    ],
                    'expanded' => true,
                    'multiple' => false,
                ]
            )
            ->add(
                'mainFieldOfResearch',
                TextType::class,
                [
                    'label' => 'IF PhD student and higher: Main field of research (required, text field, NOT published)',
                    'required' => false,
                ]
            )
            ->add(
                'publications',
                ChoiceType::class,
                [
                    'choices' => [
                        'Please select ...' => '',
                        'never' => 'never',
                        'last more than 5 years ago' => 'last more than 5 years ago',
                        'last less than 5 years ago' => 'last less than 5 years ago',
                    ],
                    'expanded' => true,
                    'multiple' => false,
                    'required' => false,
                ]
            )
            ->add(
                'coAuthoredPublicationLink',
                TextType::class,
                ['label' => ' IF PhD student (all beyond required, NOT published) AND IF NOT Dr./PhD degree: Link to (co-)authored publication', 'required' => false]
            )
            ->add(
                'staffPageLink',
                TextType::class,
                ['label' => ' IF PhD student (all beyond required, NOT published) AND IF NOT publicat.: Link to staff page with your name', 'required' => false]
            )
            ->add(
                'nameEmailOfSchoolarNowingYou',
                TextType::class,
                ['label' => ' IF PhD student (all beyond required, NOT published): AND IF NOT staff page: Name/Email of scholar knowing you', 'required' => false]
            )
            ->add(
                'programs',
                ChoiceType::class,
                [
                    'label' => 'IF PhD student (all beyond required, NOT published): Programme',
                    'choices' => [
                        'Economics' => 'Economics',
                        'Socioeconomists' => 'Socioeconomists',
                        'Philosophy & Econoimcs' => 'Philosophy & Econoimcs',
                        'Economic History' => 'Economic History',
                        'Ethics & Economics' => 'Ethics & Economics',
                        'Economic Sociology' => 'Economic Sociology',
                        'Political Economy' => 'Political Economy',
                        'Business Administration' => 'Business Administration',
                        'Other' => 'Other',
                    ],
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                ]
            )
            ->add(
                'other_programs',
                TextType::class,
                [
                    'label' => 'IF PhD student (all beyond required, NOT published): Other Programs',
                    'required' => false,
                    'empty_data' => '',
                    'data_class' => null,
                    'mapped' => false

                ]
            )
            ->add(
                'currentState',
                TextType::class,
                ['label' => 'IF PhD student (all beyond required, NOT published): Other Programs', 'required' => false, 'empty_data' => '', 'data_class' => null]
            )->add(
                'programs',
                ChoiceType::class,
                [
                    'label' => 'IF PhD student (all beyond required, NOT published): Programme',
                    'choices' => [
                        'Please select ...' => '',
                        'Economics' => 'Economics',
                        'Socioeconomists' => 'Socioeconomists',
                        'Philosophy & Econoimcs' => 'Philosophy & Econoimcs',
                        'Economic History' => 'Economic History',
                        'Ethics & Economics' => 'Ethics & Economics',
                        'Economic Sociology' => 'Economic Sociology',
                        'Political Economy' => 'Political Economy',
                        'Business Administration' => 'Business Administration',
                        'Other' => 'Other',
                    ],
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                ]
            )
            ->add(
                'association',
                ChoiceType::class,
                [
                    'label' => 'Association (required, NOT published)',
                    'required' => true,
                    'choices' => [
                        'University' => 'University',
                        'Government' => 'Government',
                        'Think Tank' => 'Think Tank',
                        'Media' => 'Media',
                        'School' => 'School',
                        'Other, pleas details below' => 'Other, pleas details below',
                    ],
                    'expanded' => true,
                    'multiple' => true,
                ]
            )
            ->add('other_association', TextType::class, [
                'label' => 'Other Association (required, NOT published)',
                'required' => false,
                'empty_data' => '',
                'data_class' => null,
                'mapped' => false
            ])
            ->add('city', TextType::class, ['label' => 'City (Required)', 'required' => true])
            ->add('country', TextType::class, ['label' => 'Country (English name, required)', 'required' => true])
            ->add('comments', TextType::class, ['label' => 'Comments (optional, NOT published)', 'empty_data' => '', 'required' => false])
            ->add('pleasInform', CheckboxType::class, ['required' => true])
            ->add(
                'isDataPolicyAccepted',
                CheckboxType::class,
                [
                    'required' => true,
                    'constraints' => [
                        new IsTrue(
                            [
                                'message' => 'I know, it\'s hard, but you must agree to our data policy.',
                            ]
                        ),
                    ],
                ]
            )
            ->add('submit', SubmitType::class, ['label' => 'Herewith I sign the letter'])
            ->getForm();

        return $form;
    }

    private function generateUuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0C2f) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0x2Aff),
            mt_rand(0, 0xffD3),
            mt_rand(0, 0xff4B)
        );
    }

    private function sendVerificationMail(\Swift_Mailer $mailer, Subscription $subscription, string $token): int
    {
        $message = (new \Swift_Message('Verification for Economists for Future Signature'))
            ->setFrom('economists.campaign@developersforfuture.org')
            ->setTo($subscription->getEmailAddress())
            ->setBody(
                $this->renderView(
                    'pages/campaign/email_verification.html.twig',
                    [
                        'subscription' => $subscription,
                        'token' => $token,
                    ]
                ),
                'text/html'
            );

        return $mailer->send($message);
    }
}
