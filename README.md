# Economists Website

## Usage Localy

clone the repository by:

```bash
# when having public key in gitlab, you can choose the ssh way
git clone git@gitlab.com:developersforfuture/economists-website.git
# or
git clone https://gitlab.com/developersforfuture/economists-website.git
# jump into the folder

cd economists-website
```

copy the dist env. variables into the local one:

```bash
cp .env.local.dist .env.local
``

read the environment variables and export them localy:

```bash
export $(egrep -v '^#' .env.development | xargs)
```

to run

```bash
docker-compose up -d
```

When running it the first time do:

```bash
docker-compose exec php sh
# you are inside the docker container now, feels like on a separate server
bin/console sulu:build dev
exit # to get out of it
```

(hint: doing so the complete dependencies are encapsulated inside a container, you do not have install anythin locally)

When switching to your browser you should be able to visit:

* http://0.0.0.0:8089 to see the homepage
* http://0.0.0.0:8089/admin to see the login of the admin panel (login: admin/admin for local usage)

Read the docs for Sulu startup now:

http://docs.sulu.io/en/2.0/book/getting-started.html
